package hw7_inheritance_polymorphism_encapsulation;

import hw7_inheritance_polymorphism_encapsulation.human.Human;
import hw7_inheritance_polymorphism_encapsulation.human.Man;
import hw7_inheritance_polymorphism_encapsulation.human.Woman;
import hw7_inheritance_polymorphism_encapsulation.pets.Dog;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class humanTest {

    @Test
    public void humanToStringTest() {
        String[][] testSchedule = {{"Monday", "sleep"},{"Tuesday", "sleep"},{"Wednesday", "sleep"},{"Thursday", "sleep"},{"Friday", "sleep"},{"Saturday", "sleep"}, {"Sunday", "sleep"}};
        Human withEmptyHuman = new Human();
        Human withNameSurnameYearHuman = new Human("John", "Travolta", 1954);
        Human withNameSurnameYearIqScheduleHuman = new Human("John", "Travolta", 1954, 99, testSchedule);

        assertEquals("human{name='undefined', surname='undefined'}", withEmptyHuman.toString(), "method human.toString");
        assertEquals("human{name='John', surname='Travolta', year=1954}", withNameSurnameYearHuman.toString(), "method human.toString");
        assertEquals("human{name='John', surname='Travolta', year=1954, iq=99, schedule=[[Monday, sleep], [Tuesday, sleep], [Wednesday, sleep], [Thursday, sleep], [Friday, sleep], [Saturday, sleep], [Sunday, sleep]]}", withNameSurnameYearIqScheduleHuman.toString(), "method human.toString for object withNameSurnameYearIqScheduleHuman");
    }

    @Test
    void familyDeleteChildByIndexTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);
        Human child4 = new Human("person4", "surname", 1986);

        hw7_inheritance_polymorphism_encapsulation.Family testFamily = new hw7_inheritance_polymorphism_encapsulation.Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);
        testFamily.addChild(child4);
        testFamily.deleteChild(2);

        Human [] expected = {child1, child2, child4};

        assertEquals(Arrays.toString(expected), Arrays.toString(testFamily.getChildren()), "method deleteChildByIndex(int Index)" );
    }

    @Test
    void familyDeleteChildByHumanTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);
        Human child4 = new Human("person4", "surname", 1986);

        hw7_inheritance_polymorphism_encapsulation.Family testFamily = new hw7_inheritance_polymorphism_encapsulation.Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);
        testFamily.addChild(child4);
        testFamily.deleteChild(child3);

        Human [] expected = {child1, child2, child4};

        assertEquals(Arrays.toString(expected), Arrays.toString(testFamily.getChildren()), "method deleteChildByHuman(human child)" );
    }

    @Test
    void familyDeleteWrongChildByIndexTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);
        Human child4 = new Human("person4", "surname", 1986);

        hw7_inheritance_polymorphism_encapsulation.Family testFamily = new hw7_inheritance_polymorphism_encapsulation.Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);
        testFamily.addChild(child4);
        testFamily.deleteChild(5);

        Human [] expected = {child1, child2, child3, child4};

        assertEquals(Arrays.toString(expected), Arrays.toString(testFamily.getChildren()), "method deleteWrongChildByIndex(Hunam Index)" );
    }

    @Test
    void familyDeleteWrongChildByHumanTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);
        Human child4 = new Human("person4", "surname", 1986);

        hw7_inheritance_polymorphism_encapsulation.Family testFamily = new hw7_inheritance_polymorphism_encapsulation.Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);
        testFamily.deleteChild(child4);

        Human [] expected = {child1, child2, child3};

        assertEquals(Arrays.toString(expected), Arrays.toString(testFamily.getChildren()), "method deleteWrongChildByHuman(Hunam WrongChild)" );
    }

    @Test
    void familyAddChildTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);

        hw7_inheritance_polymorphism_encapsulation.Family testFamily = new hw7_inheritance_polymorphism_encapsulation.Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);

        Human [] expected = {child1, child2};

        assertEquals(2, testFamily.getChildren().length, "method addChild(Hunam Child)" );
        assertEquals(Arrays.toString(expected), Arrays.toString(testFamily.getChildren()), "method deleteWrongChildByHuman(Hunam WrongChild)" );
    }

    @Test
    void familyCountFamilyTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);

        hw7_inheritance_polymorphism_encapsulation.Family testFamily = new hw7_inheritance_polymorphism_encapsulation.Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);

        assertEquals(5, testFamily.countFamily(), "method familyCountFamily()" );

    }

    @Test
    void womanBornChild() {

        Man vasya = new Man("Vasya", "Pupkin", 1956);
        Woman lena = new Woman();
        Dog barsic = new Dog("Barsic");
        Family testFamily = new Family(lena, vasya);
        testFamily.setPet(barsic);

        lena.bornChild();
        String nameChild;
        nameChild = "";

        Human[] children = testFamily.getChildren();

        for (int i = 0; i < children.length; i++) {
            if(Arrays.toString(children).contains(testFamily.getChildren()[i].getName())){
                nameChild = testFamily.getChildren()[i].getName();
            }
        }

        String expected = "human{name='" + nameChild + "', surname='Pupkin', year=2019}";

        assertEquals(expected, testFamily.getChildren()[0].toString(), "method womanBornChild()");


    }
}
