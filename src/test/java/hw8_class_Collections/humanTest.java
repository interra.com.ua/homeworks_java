package hw8_class_Collections;

import hw8_class_Collections.human.Human;
import hw8_class_Collections.human.Man;
import hw8_class_Collections.human.Woman;
import hw8_class_Collections.pets.Dog;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.EnumMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class humanTest {

    @Test
    void humanToStringTest() {
        EnumMap<DayOfWeek, String> testSchedule = new EnumMap<>(DayOfWeek.class);
        testSchedule.put(DayOfWeek.MONDAY, "monday training");
        testSchedule.put(DayOfWeek.TUESDAY, "tuesday chill");
        testSchedule.put(DayOfWeek.WEDNESDAY, "wednesday meeting");
        testSchedule.put(DayOfWeek.THURSDAY, "sleep");
        testSchedule.put(DayOfWeek.FRIDAY, "sleep");
        testSchedule.put(DayOfWeek.SATURDAY, "sleep");
        testSchedule.put(DayOfWeek.SUNDAY, "sleep");

        Human withEmptyHuman = new Human();
        Man withNameSurnameYearHuman = new Man("John", "Travolta", 1954);
        Man withNameSurnameYearIqScheduleHuman = new Man("John", "Travolta", 1954, 99, testSchedule);

        assertEquals("human{name='undefined', surname='undefined'}", withEmptyHuman.toString(), "method human.toString");
        assertEquals("human{name='John', surname='Travolta', year=1954}", withNameSurnameYearHuman.toString(), "method human.toString");
        assertEquals("human{name='John', surname='Travolta', year=1954, iq=99, schedule=[[Monday, monday training], [Tuesday, tuesday chill], [Wednesday, wednesday meeting], [Thursday, sleep], [Friday, sleep], [Saturday, sleep], [Sunday, sleep]]}", withNameSurnameYearIqScheduleHuman.toString(), "method human.toString for object withNameSurnameYearIqScheduleHuman");
    }

    @Test
    void familyAddChildTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);

        Family testFamily = new Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);

        ArrayList<Human> expected = new ArrayList<>();
        expected.add(child1);
        expected.add(child2);

        assertEquals(2, testFamily.getChildren().size(), "method addChild(Hunam Child)" );
        assertEquals(expected, testFamily.getChildren(), "method deleteWrongChildByHuman(Hunam WrongChild)" );
    }

    @Test
    void familyDeleteChildByIndexTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);
        Human child4 = new Human("person4", "surname", 1986);

        Family testFamily = new Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);
        testFamily.addChild(child4);
        testFamily.deleteChild(2);

        ArrayList<Human> expected = new ArrayList<>();
        expected.add(child1);
        expected.add(child2);
        expected.add(child4);

        assertEquals(expected, testFamily.getChildren(), "method deleteChildByIndex(int Index)" );
    }

    @Test
    void familyDeleteChildByHumanTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);
        Human child4 = new Human("person4", "surname", 1986);

        Family testFamily = new Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);
        testFamily.addChild(child4);
        testFamily.deleteChild(child3);

        ArrayList<Human> expected = new ArrayList<>();
        expected.add(child1);
        expected.add(child2);
        expected.add(child4);

        assertEquals(expected, testFamily.getChildren(), "method deleteChildByHuman(human child)" );
    }

    @Test
    void familyDeleteWrongChildByIndexTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);
        Human child4 = new Human("person4", "surname", 1986);

        Family testFamily = new Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);
        testFamily.addChild(child4);
        testFamily.deleteChild(5);

        ArrayList<Human> expected = new ArrayList<>();
        expected.add(child1);
        expected.add(child2);
        expected.add(child3);
        expected.add(child4);

        assertEquals(expected, testFamily.getChildren(), "method deleteWrongChildByIndex(Hunam Index)" );
    }

    @Test
    void familyDeleteWrongChildByHumanTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);
        Human child4 = new Human("person4", "surname", 1986);

        Family testFamily = new Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);
        testFamily.deleteChild(child4);

        ArrayList<Human> expected = new ArrayList<>();
        expected.add(child1);
        expected.add(child2);
        expected.add(child3);

        assertEquals(expected, testFamily.getChildren(), "method deleteWrongChildByHuman(Hunam WrongChild)" );
    }

    @Test
    void familyCountFamilyTest() {

        Human mother = new Human("mother", "surname", 1986);
        Human father = new Human("father", "surname", 1986);
        Human child1 = new Human("person1", "surname", 1986);
        Human child2 = new Human("person2", "surname", 1986);
        Human child3 = new Human("person3", "surname", 1986);

        Family testFamily = new Family(mother, father);
        testFamily.addChild(child1);
        testFamily.addChild(child2);
        testFamily.addChild(child3);

        assertEquals(5, testFamily.countFamily(), "method familyCountFamily()" );
    }

    @Test
    void womanBornChild() {

        Man vasya = new Man("Vasya", "Pupkin", 1956);
        Woman lena = new Woman();
        Dog barsic = new Dog("Barsic");
        Family testFamily = new Family(lena, vasya);
        testFamily.addPet(barsic);

        lena.bornChild();

        String nameChild = testFamily.getChildren().get(0).getName();
        String expected = "human{name='" + nameChild + "', surname='Pupkin', year=2019}";

        assertEquals(expected, testFamily.getChildren().get(0).toString(), "method womanBornChild()");
    }
}
