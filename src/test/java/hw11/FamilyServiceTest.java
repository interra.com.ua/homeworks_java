package hw11;

import hw12_Exceptions.family.Family;
import hw12_Exceptions.human.*;
import hw12_Exceptions.pets.Dog;
import hw12_Exceptions.pets.DomesticCat;
import hw12_Exceptions.service.FamilyService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyServiceTest {
    private static Man zorro1;
    private static Man zorro2;
    private static Man zorro3;

    private static Woman wifeOfZorro1;
    private static Woman wifeOfZorro2;
    private static Woman wifeOfZorro3;

    private static Human child1;
    private static Human child2;
    private static Human child3;

    private static Family family1;
    private static Family family2;
    private static Family family3;

    private static FamilyService familyService;


    @BeforeAll
    static void init() {
        zorro1 = new Man("Zorro1", "Family1", "15/12/1970");
        zorro2 = new Man("Zorro2", "Family2", "15/12/1970");
        zorro3 = new Man("Zorro3", "Family3", "15/12/1970");

        wifeOfZorro1 = new Woman("Elena1", "Family1", "12/05/1975");
        wifeOfZorro2 = new Woman("Elena2", "Family2", "12/05/1975");
        wifeOfZorro3 = new Woman("Elena3", "Family3", "12/05/1975");

        child1 = new Human("Person1", "Surname1", "08/04/1989");
        child2 = new Human("Person2", "Surname2", "04/06/2001");
        child3 = new Human("Person3", "Surname3", "10/11/2009");
    }


    @BeforeEach
    void createFamilies() {
        familyService = new FamilyService();

        family1 = new Family(zorro1, wifeOfZorro1);
        family2 = new Family(zorro2, wifeOfZorro2);
        family3 = new Family(zorro3, wifeOfZorro3);

        familyService.createNewFamily(zorro1, wifeOfZorro1);
        familyService.createNewFamily(zorro2, wifeOfZorro2);
        familyService.createNewFamily(zorro3, wifeOfZorro3);


    }


    @Test
    void FamilyServiceBornChildTest() {

        familyService.bornChild(zorro1.getFamily(), "VASYA", "KATYA");

        assertEquals(1, zorro1.getFamily().getChildren().size(), "method FamilyService.bornChild");
    }

    @Test
    void FamilyServiceAdoptChildTest() {
        family2.addChild(child1);
        family3.addChild(child2);
        family3.addChild(child3);

        familyService.adoptChild(zorro2.getFamily(), child1);
        familyService.adoptChild(zorro3.getFamily(), child2);
        familyService.adoptChild(zorro3.getFamily(), child3);

        ArrayList<Family> families = new ArrayList<>();
        families.add(family1);
        families.add(family2);
        families.add(family3);

        assertEquals(families.toString(), familyService.getAllFamilies().toString(), "method FamilyService.adoptChild()");
    }

    @Test
    void FamilyServiceGetFamilyByIdTest() {
        assertEquals(zorro3.getFamily(), familyService.getFamilyById(2), "method FamilyService.getFamilyById");
    }

    @Test
    void FamilyServiceGetFamiliesBiggerThanTest() {
        family2.addChild(child1);
        family3.addChild(child2);
        family3.addChild(child3);

        familyService.adoptChild(zorro2.getFamily(), child1);

        familyService.adoptChild(zorro3.getFamily(), child2);
        familyService.adoptChild(zorro3.getFamily(), child3);

        ArrayList<Family> families = new ArrayList<>();
        families.add(family2);
        families.add(family3);

        assertEquals(families.toString(), familyService.getFamiliesBiggerThan(2).toString(), "method FamilyService.getFamiliesBiggerThan");
    }

    @Test
    void FamilyServiceGetFamiliesLessThanTest() {
        family2.addChild(child1);
        family3.addChild(child2);
        family3.addChild(child3);

        familyService.adoptChild(zorro2.getFamily(), child1);
        familyService.adoptChild(zorro3.getFamily(), child2);
        familyService.adoptChild(zorro3.getFamily(), child3);


        ArrayList<Family> families = new ArrayList<>();
        families.add(family1);
        families.add(family2);


        assertEquals(families.toString(), familyService.getFamiliesLessThan(4).toString(), "method FamilyService.getFamiliesLessThan");
    }

    @Test
    void FamilyServiceCountFamiliesWithMemberNumberTest() {
        family2.addChild(child1);
        family3.addChild(child2);
        family3.addChild(child3);

        familyService.adoptChild(zorro2.getFamily(), child1);
        familyService.adoptChild(zorro3.getFamily(), child2);
        familyService.adoptChild(zorro3.getFamily(), child3);

        assertEquals(1, familyService.countFamiliesWithMemberNumber(4), "method FamilyService.countFamiliesWithMemberNumber");
    }

    @Test
    void FamilyServiceGetAllFamiliesTest() {

        ArrayList<Family> families = new ArrayList<>();
        families.add(family1);
        families.add(family2);
        families.add(family3);

        assertEquals(families.toString(), familyService.getAllFamilies().toString(), "method FamilyService.getAllFamilies");
    }

    @Test
    void FamilyServiceCountTest() {

        assertEquals(3, familyService.count(), "method FamilyService.count");
    }

    @Test
    void FamilyServiceDeleteFamilyByIndexTest() {

        familyService.deleteFamily(1);

        ArrayList<Family> families = new ArrayList<>();
        families.add(family1);
        families.add(family3);

        assertEquals(families.toString(), familyService.getAllFamilies().toString(), "method FamilyService.getFamiliesBiggerThan");
    }


    @Test
    void FamilyServiceDeleteAllChildrenOlderThenTest() {
        family3.addChild(child3);

        ArrayList<Family> families = new ArrayList<>();
        families.add(family1);
        families.add(family2);
        families.add(family3);

        familyService.adoptChild(zorro2.getFamily(), child1);

        familyService.adoptChild(zorro3.getFamily(), child2);
        familyService.adoptChild(zorro3.getFamily(), child3);
        familyService.deleteAllChildrenOlderThen(16);

        assertEquals(families.toString(), familyService.getAllFamilies().toString(), "method FamilyService.deleteAllChildrenOlderThen");
    }

    @Test
    void FamilyServiceGetPets() {
        Dog dog1 = new Dog("Polkan1", 3, 85, "play", "love", "serve");
        Dog dog2 = new Dog("Polkan2", 3, 85, "play", "love", "serve");
        DomesticCat cat = new DomesticCat("Vas'ka");

        familyService.addPet(2, dog1);
        familyService.addPet(2, dog2);
        familyService.addPet(2, cat);

        assertEquals(zorro3.getFamily().getPets(), familyService.getPets(2), "method FamilyService.getPets");
    }
}
