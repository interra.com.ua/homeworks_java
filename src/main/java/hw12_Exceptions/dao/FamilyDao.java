package hw12_Exceptions.dao;

import hw12_Exceptions.family.Family;

import java.util.List;

public interface FamilyDao {

    void addFamily(Family o);

    List getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family o);

    void saveFamily(Family o);
}
