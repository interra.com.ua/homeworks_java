package hw12_Exceptions.сontroller;

import hw12_Exceptions.exeptions.FamilyOverflowException;
import hw12_Exceptions.family.Family;
import hw12_Exceptions.human.Human;
import hw12_Exceptions.human.Man;
import hw12_Exceptions.human.Woman;
import hw12_Exceptions.pets.Pet;
import hw12_Exceptions.service.FamilyService;

import java.util.ArrayList;
import java.util.HashSet;

public class FamilyController {
    private FamilyService familyService = new FamilyService();

    public void createNewFamily(Man father, Woman mother) {
        familyService.createNewFamily(father, mother);
    }

//    create

    public Family adoptChild(Family o, Human child) {
        try {
            familyService.adoptChild(o, child);
        } catch (FamilyOverflowException ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getfamily().prettyFormat());
        }
        return o;
    }

//    public Family adoptChild(Family o, Woman child) {
//        return familyService.adoptChild(o, child);
//    }

    public Family bornChild(Family o, String boysName, String femaleName) {
        try {
            familyService.bornChild(o, boysName, femaleName);
        } catch (FamilyOverflowException ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getfamily().prettyFormat());
        }
        return o;
    }

    public void addPet(int indexFamily, Pet o) {
        familyService.addPet(indexFamily, o);
    }

//    read

    public Family getFamilyById(int familyIndex) {
        return familyService.getFamilyById(familyIndex);
    }

    public ArrayList<Family> getFamiliesBiggerThan(int val) {
        return familyService.getFamiliesBiggerThan(val);
    }

    public ArrayList<Family> getFamiliesLessThan(int val) {
        return familyService.getFamiliesLessThan(val);
    }

    public ArrayList<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public int count() {
        return familyService.count();
    }

    public int countFamiliesWithMemberNumber(int val) {
        return familyService.countFamiliesWithMemberNumber(val);
    }

    public HashSet<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }


//    delete

    public void deleteFamily(int index) {
        familyService.deleteFamily(index);
    }

    public void deleteFamily(Family o) {
        familyService.deleteFamily(o);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

//    update

    public void saveFamily(Family o) {
        familyService.saveFamily(o);
    }

}
