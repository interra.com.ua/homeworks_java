package hw12_Exceptions.family;

import hw12_Exceptions.exeptions.FamilyOverflowException;
import hw12_Exceptions.human.Human;
import hw12_Exceptions.human.Man;
import hw12_Exceptions.human.Woman;
import hw12_Exceptions.pets.Pet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;

public class Family {
    private Woman mother;
    private Man father;
    private ArrayList<Human> children = new ArrayList<>();
    private HashSet<Pet> pets = new HashSet<>();

    public Family(Man father, Woman mother) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);

    }

    @Override
    public String toString() {
        StringBuilder family = new StringBuilder();

        family.append("family{mother=").append(mother).append(", father=").append(father);

        if (!children.isEmpty()) {
            family.append(children);
        }
        if (!pets.isEmpty()) {
            family.append(pets);
        }

        return family.toString();
    }

    public String humanGetPrettyString(Human o) {
        String family = o.toString();
        int index = family.indexOf("{");
        return family.substring(index);
    }

    public String prettyFormat() {
        String family;
        family = String.format("family:%n");
        family += String.format("%15s %s%n", "mother:", humanGetPrettyString(mother));
        family += String.format("%15s %s%n", "father:", humanGetPrettyString(father));
        if (!children.isEmpty()) {
            family += String.format("%17s%n", "children:");
            family += children.stream()
                    .map(o -> String.format("%21s %s%n", "child", humanGetPrettyString(o)))
                    .collect(Collectors.joining());
        }

        return family;

//        return humanGetPrettyString(mother) + " " + humanGetPrettyString(children.get(0));
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    public Woman getMother() {
        return mother;
    }

    public Man getFather() {
        return father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void addChild(Human child) throws FamilyOverflowException {
        if (children.size() < 5) {
            children.add(child);
            child.setFamily(this);
        } else {
            throw new FamilyOverflowException("Quantity of children has already reached maximum size (5 persons)!", this);
        }
    }

    public boolean deleteChild(int index) {
        return index >= 0 && index < children.size() && children.remove(index) != null;
    }

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
        pet.setFamily(this);
    }

    public HashSet<Pet> getPets() {
        return pets;
    }

    public int countFamily() {
        return 2 + children.size();
    }
}
