package hw12_Exceptions.family;

import hw12_Exceptions.dao.FamilyDao;

import java.util.ArrayList;

public class CollectionFamilyDao implements FamilyDao {
    private ArrayList<Family> bd = new ArrayList<>();

    @Override
    public void addFamily(Family o) {
        bd.add(o);
    }

    @Override
    public ArrayList<Family> getAllFamilies() {
        return bd;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return bd.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index <= bd.size()) {
            bd.remove(index);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteFamily(Family o) {
        if (bd.contains(o)) {
            bd.remove(o);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void saveFamily(Family o) {
        if (bd.contains(o)) {
            int index = bd.indexOf(o);
            bd.set(index, o);
        } else {
            bd.add(o);
        }
    }
}
