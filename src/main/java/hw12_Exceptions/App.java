package hw12_Exceptions;

import hw12_Exceptions.family.Family;
import hw12_Exceptions.human.Human;
import hw12_Exceptions.human.Man;
import hw12_Exceptions.human.Woman;
import hw12_Exceptions.сontroller.FamilyController;

import java.util.ArrayList;
import java.util.Scanner;

public class App {

    private FamilyController fc = new FamilyController();
    private Scanner stringInput = new Scanner(System.in);
    private Scanner intInput = new Scanner(System.in);


    private void displayCommands() {
        System.out.printf("Please, enter the command's number or \"exit\" %n" +
                "\"1\" Create 3 default families %n" +
                "\"2\" Display families' list %n" +
                "\"3\" Display families' list with number of members more than %n" +
                "\"4\" Display families' list with number of members less than %n" +
                "\"5\" Count number of families with a given number of members %n" +
                "\"6\" Create new family %n" +
                "\"7\" Delete family by ID %n" +
                "\"8\" Edit family by ID %n" +
                "\"9\" Delete all children older than %n" +
                "\"exit\" for complite %n");
    }

    private void createThreeDefaultFamilies() {
        Man zorro1 = new Man("Zorro1", "Family1", "15/12/1970");
        Man zorro2 = new Man("Zorro2", "Family2", "15/12/1970");
        Man zorro3 = new Man("Zorro3", "Family3", "15/12/1970");

        Woman wifeOfZorro1 = new Woman("Elena1", "Family1", "12/05/1975");
        Woman wifeOfZorro2 = new Woman("Elena2", "Family2", "12/05/1975");
        Woman wifeOfZorro3 = new Woman("Elena3", "Family3", "12/05/1975");

        Human child1 = new Human("Person1", "Surname1", "08/04/1989");
        Human child2 = new Human("Person2", "Surname2", "04/06/2001");
        Human child3 = new Human("Person3", "Surname3", "10/11/2009");

        fc.createNewFamily(zorro1, wifeOfZorro1);
        fc.createNewFamily(zorro2, wifeOfZorro2);
        fc.createNewFamily(zorro3, wifeOfZorro3);
        fc.adoptChild(zorro2.getFamily(), child1);
        fc.adoptChild(zorro3.getFamily(), child2);
        fc.adoptChild(zorro3.getFamily(), child3);
    }

    private void FillingFieldsNewHuman(Human o) {
        String year;
        String month;
        String date;

        o.setName(askClient("The name:"));
        o.setSurname(askClient("The surname:"));
        o.setIq(Integer.parseInt(askClient("iq:")));

        year = askClient("The year of birth (yyyy):");
        month = askClient("The month of birth (mm):");
        date = askClient("The date of birth (dd):");

        o.setBirthDate(year, month, date);
    }

    private String askClient(String question) {
        System.out.println(question);
        return stringInput.nextLine();
    }

    void runApp() {

        String inputtedCommand;

        do {
            label:
            {
                this.displayCommands();
                inputtedCommand = stringInput.nextLine();
                switch (inputtedCommand) {
                    case "1":
                        this.createThreeDefaultFamilies();
                        break;
                    case "2":
                        System.out.println("************************All families*********************");
                        fc.getAllFamilies().stream().map(family -> family.prettyFormat()).forEach(System.out::print);
                        System.out.println("*********************************************************");
                        break;
                    case "3":
                        System.out.println("Enter wanted number of members:");
                        ArrayList<Family> familiesBiggerThan = fc.getFamiliesBiggerThan(intInput.nextInt());
                        System.out.println("**********Families bigger than*********");
                        familiesBiggerThan.stream().map(family -> family.prettyFormat()).forEach(System.out::print);
                        System.out.println("*********************************************************");
                        break;
                    case "4":
                        System.out.println("Enter wanted number of members:");
                        ArrayList<Family> familiesLessThan = fc.getFamiliesLessThan(intInput.nextInt());
                        System.out.println("*********************Families less than******************");
                        familiesLessThan.stream().map(family -> family.prettyFormat()).forEach(System.out::print);
                        System.out.println("*********************************************************");
                        break;
                    case "5":
                        System.out.println("Enter wanted number of members:");
                        int numberOfFamilies = fc.countFamiliesWithMemberNumber(intInput.nextInt());
                        System.out.println("**Number of families with the desired number of members**");
                        System.out.println(numberOfFamilies);
                        System.out.println("*********************************************************");
                        break;
                    case "6":
                        System.out.println("Please, enter mother's data!");
                        Woman mother = new Woman();
                        FillingFieldsNewHuman(mother);
                        System.out.println("Please, enter father's data!");
                        Man father = new Man();
                        FillingFieldsNewHuman(father);
                        fc.createNewFamily(father, mother);
                        break;
                    case "7":
                        int id = Integer.parseInt(askClient("Enter ID:"));
                        fc.deleteFamily(id);
                        break;
                    case "8":
                        System.out.printf("Please, enter the command's number or \"exit\" %n" +
                                "\"1\" Bear child in choosen family %n" +
                                "\"2\" Adopt child in choosen family %n" +
                                "\"3\" Back to the main menu %n");
                        inputtedCommand = stringInput.nextLine();
                        switch (inputtedCommand) {
                            case "1":
                                id = Integer.parseInt(askClient("Enter ID:"));
                                String boysName = askClient("Enter boy's name");
                                String girlsName = askClient("Enter girl's name");
                                fc.bornChild(fc.getFamilyById(id), boysName, girlsName);
                                break label;
                            case "2":
                                id = Integer.parseInt(askClient("Enter ID:"));
                                Human child = new Human();
                                FillingFieldsNewHuman(child);
                                fc.getFamilyById(id).addChild(child);
//                                String sexOfChild = (askClient(String.format("What gender is the child you want to adopt?%n \"1\" boy%n \"2\" girl%n ")));
//                                if(sexOfChild.equals("1")){
//                                    Man child = new Man();
//                                    FillingFieldsNewHuman(child);
//                                    fc.adoptChild(fc.getFamilyById(id), child);
//                                } else if(sexOfChild.equals("2")){
//                                    Woman child = new Woman();
//                                    FillingFieldsNewHuman(child);
//                                    fc.adoptChild(fc.getFamilyById(id), child);
//                                } else throw new IllegalStateException("Unexpected value: " + inputtedCommand);
                                break label;
                            case "3":
                                break label;
                            default:
                                throw new IllegalStateException("Unexpected value: " + inputtedCommand);
                        }
                    case "9":
                        String age = askClient("Enter age:");
                        fc.deleteAllChildrenOlderThen(Integer.parseInt(age));
                        break;
                    default:
                        if (!inputtedCommand.equalsIgnoreCase("Exit")) {
                            throw new IllegalStateException("Unexpected value: " + inputtedCommand);
                        }

                }

            }
        } while (!inputtedCommand.equalsIgnoreCase("Exit"));

    }


}
