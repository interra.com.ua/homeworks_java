package hw12_Exceptions.service;

import hw12_Exceptions.exeptions.FamilyOverflowException;
import hw12_Exceptions.family.CollectionFamilyDao;
import hw12_Exceptions.family.Family;
import hw12_Exceptions.human.Human;
import hw12_Exceptions.human.Man;
import hw12_Exceptions.human.Woman;
import hw12_Exceptions.pets.Pet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    private CollectionFamilyDao familyDao = new CollectionFamilyDao();

//    create

    public void createNewFamily(Man father, Woman mother) {
        Family family = new Family(father, mother);
        familyDao.addFamily(family);
    }

    public Family bornChild(Family o, String boysName, String femaleName) throws FamilyOverflowException {
        o.getMother().bornChild(boysName, femaleName);
        return o;
    }

    public Family adoptChild(Family o, Human child) throws FamilyOverflowException{
        o.addChild(child);
        return o;
    }

    public void addPet(int indexFamily, Pet o) {
        familyDao.getAllFamilies().get(indexFamily - 1).addPet(o);
    }

//    read

    public Family getFamilyById(int familyIndex) {
        return familyDao.getAllFamilies().get(familyIndex - 1);
    }

    public ArrayList<Family> getFamiliesBiggerThan(int val) {
        ArrayList<Family> familiesBiggerThan = new ArrayList<>();

        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > val)
                .forEach(familiesBiggerThan::add);

        return familiesBiggerThan;
    }

    public ArrayList<Family> getFamiliesLessThan(int val) {
        ArrayList<Family> familiesLessThan = new ArrayList<>();

        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < val)
                .forEach(familiesLessThan::add);

        return familiesLessThan;
    }

    public ArrayList<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {

        List<Family> families = familyDao.getAllFamilies();


        families.forEach(family -> {
            String father = "Father: " + family.getFather().getName() + " " + family.getFather().getSurname();
            String mother = " mother: " + family.getMother().getName() + " " + family.getMother().getSurname();
            String children = family.getChildren().stream()
                    .map(child -> (", child: " + child.getName() + " " + child.getSurname()))
                    .collect(Collectors.joining());

            System.out.println(families.indexOf(family) + 1 + "). " + father + mother + " " + children);
        });
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public int countFamiliesWithMemberNumber(int val) {

        return (int) familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() == val)
                .count();
    }

    public HashSet<Pet> getPets(int familyIndex) {
        return familyDao.getAllFamilies().get(familyIndex).getPets();
    }

//    delete

    public void deleteFamily(int index) {
        familyDao.deleteFamily(index - 1);
    }

    public void deleteFamily(Family o) {
        familyDao.deleteFamily(o);
    }

    public void deleteAllChildrenOlderThen(int age) {

        familyDao.getAllFamilies().forEach(family -> {
            ArrayList<Human> childrenForDelete = new ArrayList();

            family.getChildren().stream()
                    .filter(child -> (Integer.valueOf(child.describeAge().split("y")[0]) > age))
                    .forEach(childrenForDelete::add);
            family.getChildren().removeAll(childrenForDelete);

        });
    }

//    update

    public void saveFamily(Family o) {
        familyDao.saveFamily(o);
    }
}
