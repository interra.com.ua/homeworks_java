package hw12_Exceptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {
    private Date date;
    SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");

    public DateFormatter(String date) {
        Date parsingDate;

        try {
            parsingDate = ft.parse(date);
            this.date = parsingDate;
        } catch (ParseException e) {
            System.out.println("Did not parsed with " + ft);
        }
    }

    public DateFormatter(String year, String month, String day) {
        this(day + "/" + month + "/" + year);
    }

    public DateFormatter(long msec) {
        this.date = new Date(msec);
    }

    public long getMsec() {
        return date.getTime();
    }

    @Override
    public String toString() {
        return ft.format(date);
    }


}
