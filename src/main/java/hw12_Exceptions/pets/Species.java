package hw12_Exceptions.pets;

public enum Species {
    DOG,
    DOMESTICCAT,
    FISH,
    BIRD,
    RABBIT,
    TURTLE,
    SNAKE,
    ROBOCAT,
    UNKNOWN
}
