package hw12_Exceptions.human;

import hw12_Exceptions.DayOfWeek;
import hw12_Exceptions.family.Family;
import hw12_Exceptions.pets.Pet;

import java.util.EnumMap;

final public class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, String birthDate, int iq, Family family) {
        super(name, surname, birthDate, iq, family);
    }

    public Man(String name, String surname, String birthDate, int iq, EnumMap<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    @Override
    public void greetPets() {
        if (family != null && !family.getPets().isEmpty()) {
            StringBuilder nickNamesList = new StringBuilder();
            for (Pet pet : family.getPets()) {
                nickNamesList.append(pet.getNickname()).append(", ");
            }
            System.out.println("Hi, \" + nickNamesList + \"would you like food?");
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void repairCar() {
        System.out.println("It is done! The car is repiared!");
    }
}
