package hw12_Exceptions.human;

import hw12_Exceptions.DateFormatter;
import hw12_Exceptions.DayOfWeek;
import hw12_Exceptions.family.Family;
import hw12_Exceptions.pets.Pet;

import java.util.*;

public class Human {
    protected String name;
    protected String surname;
    protected long birthDate;
    protected int iq;
    protected EnumMap<DayOfWeek, String> schedule;
    protected Family family;

    public Human() {
    }

    public Human(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;

        DateFormatter date = new DateFormatter(birthDate);
        this.birthDate = date.getMsec();

    }

    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.iq = iq;

        DateFormatter date = new DateFormatter(birthDate);
        this.birthDate = date.getMsec();

    }

    public Human(String name, String surname, String birthDate, int iq, Family family) {
        this(name, surname, birthDate);
        this.iq = iq;
        this.family = family;
    }

    public Human(String name, String surname, String birthDate, int iq, EnumMap<DayOfWeek, String> schedule) {
        this(name, surname, birthDate);
        this.iq = iq;
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        StringBuilder human = new StringBuilder();
        human.append("human{");
        if (name != null) {
            human.append("name='").append(name).append("'");
        } else human.append("name='undefined'");
        if (surname != null) {
            human.append(", surname='").append(surname).append("'");
        } else human.append(", surname='undefined'");
        if (birthDate != 0) {
            DateFormatter date = new DateFormatter(birthDate);
            human.append(", birthDate=").append(date.toString());
        }
        if (iq != 0) {
            human.append(", iq=").append(getIq());
        }
        if (schedule != null && !schedule.isEmpty()) {
            human.append(", schedule=[").append(getSchedule()).append("]");
        }
        human.append("}");

        return human.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;
        Human human = (Human) obj;
        return (name.equals(human.name) && surname.equals(human.surname) && birthDate == human.birthDate);
    }

    @Override
    public int hashCode() {
        int code = 11;
        int k = 7;
        code = k * code + name.hashCode() + surname.hashCode() + (int) birthDate;
        return code;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public void setBirthDate(String year, String month, String day) {
        DateFormatter date = new DateFormatter(year, month, day);
        this.birthDate = date.getMsec();
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public int getIq() {
        return iq;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public String describeAge() {

        GregorianCalendar calendarBirthday = new GregorianCalendar();
        calendarBirthday.setTimeInMillis(birthDate);

        GregorianCalendar calendarNow = new GregorianCalendar();
        int year;
        int month;
        int day;

        if (calendarNow.get(Calendar.YEAR) == calendarBirthday.get(Calendar.YEAR)) {
            year = 0;
        } else if (calendarNow.get(Calendar.MONTH) <= calendarBirthday.get(Calendar.MONTH) && calendarNow.get(Calendar.DAY_OF_MONTH) < calendarBirthday.get(Calendar.DAY_OF_MONTH)) {
            year = calendarNow.get(Calendar.YEAR) - calendarBirthday.get(Calendar.YEAR) - 1;
        } else {
            year = calendarNow.get(Calendar.YEAR) - calendarBirthday.get(Calendar.YEAR);
        }

        if (calendarNow.get(Calendar.MONTH) < calendarBirthday.get(Calendar.MONTH)) {
            month = 12 - (calendarBirthday.get(Calendar.MONTH) - calendarNow.get(Calendar.MONTH));
        } else if (calendarNow.get(Calendar.MONTH) == calendarBirthday.get(Calendar.MONTH)) {
            if (calendarNow.get(Calendar.DAY_OF_MONTH) < calendarBirthday.get(Calendar.DAY_OF_MONTH)) {
                month = 11;
            } else {
                month = 0;
            }
        } else {
            if (calendarNow.get((Calendar.DAY_OF_MONTH)) < calendarBirthday.get(Calendar.DAY_OF_MONTH)) {
                month = calendarNow.get(Calendar.MONTH) - calendarBirthday.get(Calendar.MONTH) - 1;
            } else {
                month = calendarNow.get(Calendar.MONTH) - calendarBirthday.get(Calendar.MONTH);
            }
        }

        GregorianCalendar birthdayDay = new GregorianCalendar();
        birthdayDay.set(Calendar.DAY_OF_MONTH, calendarBirthday.get(Calendar.DAY_OF_MONTH));

        if (calendarNow.before(birthdayDay)) {

            birthdayDay.add(Calendar.MONTH, -1);
            day = (int) ((calendarNow.getTimeInMillis() - birthdayDay.getTimeInMillis()) / 1000 / 60 / 60 / 24);
        } else {
            day = (int) ((calendarNow.getTimeInMillis() - birthdayDay.getTimeInMillis()) / 1000 / 60 / 60 / 24);
        }

        return (year + "y, " + month + "m, " + day + "d ");
    }

    public String getSchedule() {
        String scheduleToString = "";
        int i = 0;

        for (Map.Entry<DayOfWeek, String> keyValue : schedule.entrySet()) {
            i++;
            scheduleToString = scheduleToString + "[" + keyValue.getKey().toString().substring(0, 1).toUpperCase() + keyValue.getKey().toString().substring(1).toLowerCase() + ", " + keyValue.getValue();
            if (i == schedule.entrySet().size()) {
                scheduleToString = scheduleToString + "]";
            } else {
                scheduleToString = scheduleToString + "], ";
            }
        }
        return scheduleToString;
    }

    public void greetPets() {
        if (family != null && !family.getPets().isEmpty()) {
            String nickNamesList = "";
            for (Pet pet : family.getPets()) {
                nickNamesList = nickNamesList + pet.getNickname() + ", ";
            }

            System.out.println("Hello, " + nickNamesList + "! How are you?");
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void describePets() {
        if (family != null && !family.getPets().isEmpty()) {
            for (Pet pet : family.getPets()) {
                String species = pet.getSpecies().toString().substring(0, 1).toUpperCase() + pet.getSpecies().toString().substring(1).toLowerCase();
                System.out.printf("I have %s, it is %d years old, it is %s.%n", species, pet.getAge(), ((pet.getTrickLevel() > 50) ? "very cunning" : "almost not cunning"));
            }
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }

    }

    public void feedPet(Pet pet) {
        if (family != null && !family.getPets().isEmpty() && family.getPets().contains(pet)) {
            System.out.printf("My pet %s, what are you doing? %n", pet.getNickname());
            pet.eat();
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }

    }
}
