package hw12_Exceptions.human;

import hw12_Exceptions.DayOfWeek;
import hw12_Exceptions.exeptions.FamilyOverflowException;
import hw12_Exceptions.family.Family;
import hw12_Exceptions.pets.Pet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumMap;
import java.util.Random;

final public class Woman extends Human implements HumanCreate {

    public Woman() {
    }

    public Woman(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }



    public Woman(String name, String surname, String birthDate, int iq, Family family) {
        super(name, surname, birthDate, iq, family);
    }

    public Woman(String name, String surname, String birthDate, int iq, EnumMap<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    @Override
    public void greetPets() {
        if(family != null && !family.getPets().isEmpty() ) {
            StringBuilder nickNamesList = new StringBuilder();
            for (Pet pet : family.getPets()) {
                nickNamesList.append(pet.getSpecies().toString().toLowerCase()).append(" ").append(pet.getNickname()).append(", ");
            }

            System.out.println("Hello, \" + nickNamesList + \", I love you!");
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void makeup(){
        System.out.println("Great! Now I am even more beautiful");
    }

    @Override
    public Human bornChild(String boysName, String femaleName) throws FamilyOverflowException {
        Random random = new Random();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd/MM/yyyy");

        Date now = new Date();
        String birthDate = formatForDateNow.format(now);
        Human addedChild;

        if(random.nextInt(101)%2 > 0){
            int iqOfChild = (this.family.getFather().getIq() + iq)/2;
            Man child = new Man(boysName, this.family.getFather().surname, birthDate, iqOfChild, this.family);
            this.family.addChild(child);
            addedChild = child;
        } else {
            int iqOfChild = (this.family.getFather().getIq() + this.iq)/2;
            Woman child = new Woman(femaleName, this.family.getFather().surname, birthDate, iqOfChild, this.family);
            this.family.addChild(child);
            addedChild = child;
        }
        return addedChild;
    }
}
