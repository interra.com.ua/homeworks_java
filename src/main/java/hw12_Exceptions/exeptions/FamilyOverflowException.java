package hw12_Exceptions.exeptions;

import hw12_Exceptions.family.Family;

public class FamilyOverflowException extends RuntimeException{
    private Family family;

    public FamilyOverflowException(String message, Family family){
        super(message);
        this.family = family;
    }

    public Family getfamily() {
        return this.family;
    }
}
