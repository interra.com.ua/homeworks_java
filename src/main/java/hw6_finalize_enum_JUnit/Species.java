package hw6_finalize_enum_JUnit;

public enum Species {
    DOG,
    CAT,
    FISH,
    BIRD,
    RABBIT,
    TURTLE,
    SNAKE;
}
