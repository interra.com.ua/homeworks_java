package hw6_finalize_enum_JUnit;

import java.util.Arrays;

class Main {

    public static void main(String[] args) {

        String[][] h = {{DayOfWeek.MONDAY.name(), "sleep"},{DayOfWeek.TUESDAY.name(), "sleep"},{DayOfWeek.WEDNESDAY.name(), "sleep"},{DayOfWeek.THURSDAY.name(), "sleep"},{DayOfWeek.FRIDAY.name(), "sleep"},{DayOfWeek.SATARDEY.name(), "sleep"}, {DayOfWeek.SUNDAY.name(), "sleep"}};
        Pet beethoven = new Pet(Species.DOG, "Beethoven", 7, 85, "slobber", "save children", "sleep with family");

        Human zorro = new Human("Zorro", "de la Vega", 1970 ,85, h);
        Human wifeOfZorro = new Human("Elena", "de la Vega", 1975);
        Human daughterOfZorro = new Human("Alice", "de la Vega", 1997);
        Human sunOfZorro = new Human("Hoakin", "de la Vega", 2000, 86, h);

        Family deLaVega = new Family(wifeOfZorro, zorro);
        System.out.println(deLaVega.toString());

        deLaVega.setPet(beethoven);
        System.out.println(deLaVega.toString());

        deLaVega.addChild(daughterOfZorro);
        deLaVega.addChild(sunOfZorro);
        deLaVega.setPet(beethoven);
        System.out.println(deLaVega.toString());
        System.out.println("Количество членов семьи: " + deLaVega.countFamily());

        System.out.printf("%n ****** %n%n");

        System.out.println(Arrays.toString(deLaVega.getChildren()));
        deLaVega.deleteChild(0);
        System.out.println(Arrays.toString(deLaVega.getChildren()));
        System.out.println(deLaVega.toString());
        System.out.println("Количество членов семьи: " + deLaVega.countFamily());

        System.out.printf("%n ****** %n%n");

        zorro.greetPet();
        daughterOfZorro.describePet();
        wifeOfZorro.feedPet();

        System.out.printf("%n ****** %n%n");

        System.out.println("Zorro это Zorro? " + zorro.equals(zorro));
        System.out.println("Zorro это отец семьи de la Vega? " + zorro.equals(deLaVega.getFather()));
        System.out.println("Отец семьи de la Vega - это Zorro? " + deLaVega.getFather().equals(zorro));
        System.out.println("Является ли Zorro матерью семьи de la Vega? " + zorro.equals(deLaVega.getMother()));



        for (int i = 0; i<1000000; i++){
            Human person = new Human("Vasya", "Pupkin", 1986);
        }

        String word = "hello";
        word = word + " world";
        System.out.println(word);


    }
}
