package hw6_finalize_enum_JUnit;

import java.util.Arrays;

class Family {
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;

    Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);

    }

    @Override
    public String toString() {
        StringBuilder family = new StringBuilder();

        family.append("family{" +
                "mother=" + mother +
                ", father=" + father);
        if (children.length != 0){
            family.append(Arrays.toString(children));
        }
        if (pet != null){
            family.append(pet);
        }

        return family.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    Human getMother() {
        return mother;
    }

    Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    void setPet(Pet pet) {
        this.pet = pet;
    }

    Pet getPet() {
        return pet;
    }

    void addChild(Human child){
        if (children != null){
            Human[] array = new Human[children.length + 1];
            System.arraycopy(children, 0, array, 0, children.length);
            array[array.length - 1] = child;
            children = array;
        } else {
            children = new Human[]{child};
        }

       child.setFamily(this);
    }

    boolean deleteChild(int index){

        if (children != null && index >= 0 && index < children.length){
            Human[] array = new Human[children.length - 1];
            for(int i = 0; i < array.length; i++){
                if(i < index){
                    array[i] = children[i];
                } else {
                    array[i] = children[i + 1];
                }
            }
            children = array;
            return true;
        }
        return false;
    }

    boolean deleteChild(Human child){

        if (children != null){
            for (int i = 0; i < children.length; i++) {
                if (children[i].equals(child)) {
                    Human[] array = new Human[children.length - 1];
                    System.arraycopy(children, 0, array, 0, i);
                    System.arraycopy(children, (i + 1), array, i, (array.length - i));
                    children = array;
                    return true;
                }
            }
        }
        return false;
    }



    int countFamily(){
        return 2 + children.length;
    }
}
