package hw10_date_and_time.pets;

public enum Species {
    DOG,
    DOMESTICCAT,
    FISH,
    BIRD,
    RABBIT,
    TURTLE,
    SNAKE,
    ROBOCAT,
    UNKNOWN
}
