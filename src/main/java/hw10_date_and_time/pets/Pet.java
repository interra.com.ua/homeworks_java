package hw10_date_and_time.pets;

import hw10_date_and_time.family.Family;

import java.util.HashSet;
import java.util.Objects;

public abstract class Pet {
    protected Species species = Species.UNKNOWN;
    protected String nickname;
    protected int age;
    protected int trickLevel;
    protected HashSet<String> habits;
    protected Family family;

    public Pet() {
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, String... habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        for (int i = 0; i < habits.length; i++) {
            if (i == 0) {
                this.habits = new HashSet<>();
            }
            this.habits.add(habits[i]);
        }
    }

    public Pet(String nickname, int age, int trickLevel, HashSet<String> habits, Family family) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.family = family;
    }

    @Override
    public String toString() {
        StringBuilder pet = new StringBuilder();
        pet.append(species + "{");
        if (!nickname.isEmpty()) {
            pet.append("nickname='" + nickname + "'");
        }
        if (age != 0) {
            pet.append(", age=" + age);
        }
        if (trickLevel != 0) {
            pet.append(", trickLevel=" + trickLevel);
        }
        if (habits != null && habits.size() > 0) {
            pet.append(", habits=" + habits.toString());
        }
        pet.append("}");

        return pet.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return getSpecies().equals(pet.getSpecies()) &&
                getNickname().equals(pet.getNickname()) &&
                Objects.equals(getFamily(), pet.getFamily());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSpecies(), getNickname());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    public String getNickname() {
        return nickname;
    }

    public Species getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public void eat() {
        System.out.println("I am eating!");
    }

    public abstract void respond();
}
