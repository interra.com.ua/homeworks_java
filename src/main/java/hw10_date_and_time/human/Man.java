package hw10_date_and_time.human;

import hw10_date_and_time.DayOfWeek;
import hw10_date_and_time.family.Family;
import hw10_date_and_time.pets.Pet;

import java.util.EnumMap;

final public class Man extends Human {

    public Man(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, int iq, Family family) {
        super(name, surname, birthDate, iq, family);
    }

    public Man(String name, String surname, String birthDate, int iq, EnumMap<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    @Override
    public void greetPets() {
        if (family != null && !family.getPets().isEmpty()) {
            String nickNamesList = "";
            for (Pet pet : family.getPets()) {
                nickNamesList = nickNamesList + pet.getNickname() + ", ";
            }
            System.out.println("Hi, \" + nickNamesList + \"would you like food?");
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void repairCar() {
        System.out.println("It is done! The car is repiared!");
    }
}
