package hw10_date_and_time;

import hw10_date_and_time.human.*;

class Main {

    public static void main(String[] args) {
        Human buddy = new Human("Macaulay", "Culkin", "20/08/1980");
        System.out.println(buddy);
        System.out.println("McCaley lived full " + buddy.describeAge());
    }
}
