package hw10_date_and_time.сontroller;

import hw10_date_and_time.family.Family;
import hw10_date_and_time.service.FamilyService;
import hw10_date_and_time.human.*;
import hw10_date_and_time.pets.Pet;

import java.util.ArrayList;
import java.util.HashSet;

public class FamilyController {
    FamilyService familyService = new FamilyService();

    public void createNewFamily(Man father, Woman mother) {
        familyService.createNewFamily(father, mother);
    }

//    create

    public Family adoptChild(Family o, Human child) {
        return familyService.adoptChild(o, child);
    }

    public Family bornChild(Family o, BoysNames boysName, GirlsNames femaleName) {
        return familyService.bornChild(o, boysName, femaleName);
    }

    public void addPet(int indexFamily, Pet o) {
        familyService.addPet(indexFamily, o);
    }

//    read

    public Family getFamilyById(int familyIndex) {
        return familyService.getFamilyById(familyIndex);
    }

    public ArrayList<Family> getFamiliesBiggerThan(int val) {
        return familyService.getFamiliesBiggerThan(val);
    }

    public ArrayList<Family> getFamiliesLessThan(int val) {
        return familyService.getFamiliesLessThan(val);
    }

    public ArrayList<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public int count() {
        return familyService.count();
    }

    public int countFamiliesWithMemberNumber(int val) {
        return familyService.countFamiliesWithMemberNumber(val);
    }

    public HashSet<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }


//    delete

    public void deleteFamily(int index) {
        familyService.deleteFamily(index);
    }

    public void deleteFamily(Family o) {
        familyService.deleteFamily(o);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

//    update

    public void saveFamily(Family o) {
        familyService.saveFamily(o);
    }

}
