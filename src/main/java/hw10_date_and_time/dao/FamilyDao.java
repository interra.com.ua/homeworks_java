package hw10_date_and_time.dao;

import hw10_date_and_time.family.Family;

import java.util.List;

public interface FamilyDao {

    void addFamily(Family o);

    List getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family o);

    void saveFamily(Family o);
}
