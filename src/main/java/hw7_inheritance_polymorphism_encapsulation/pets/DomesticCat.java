package hw7_inheritance_polymorphism_encapsulation.pets;

import hw7_inheritance_polymorphism_encapsulation.Family;

public class DomesticCat extends Pet implements Foul {

    public DomesticCat(String nickname) {
        super(nickname);
        super.species = Species.DOMESTICCAT;
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
        super.species = Species.DOMESTICCAT;
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am the cat " + super.nickname + ". Why do you stand and look at me? Have you brought Whiskas?..");
    }

    @Override
    public void foul() {
        System.out.println("Okay I slept now I can go demand food...");
    }
}
