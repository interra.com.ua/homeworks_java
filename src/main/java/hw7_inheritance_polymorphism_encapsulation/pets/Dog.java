package hw7_inheritance_polymorphism_encapsulation.pets;

import hw7_inheritance_polymorphism_encapsulation.Family;

public class Dog extends Pet implements Foul {
    public Dog(String nickname) {
        super(nickname);
        super.species = Species.DOG;
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
        super.species = Species.DOG;
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am the dog " + nickname + ". I've missed you!");
    }

    @Override
    public void foul() {
        System.out.println("I need to cover the evidence well...");
    }
}
