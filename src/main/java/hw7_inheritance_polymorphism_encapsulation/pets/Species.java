package hw7_inheritance_polymorphism_encapsulation.pets;

public enum Species {
    DOG,
    DOMESTICCAT,
    FISH,
    BIRD,
    RABBIT,
    TURTLE,
    SNAKE,
    ROBOCAT,
    UNKNOWN
}
