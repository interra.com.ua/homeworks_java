package hw7_inheritance_polymorphism_encapsulation.human;

import hw7_inheritance_polymorphism_encapsulation.Family;

final public class Man extends Human {

    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Family family) {
        super(name, surname, year, iq, family);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet() {
        if (family != null && family.getPet() != null) {
            System.out.println("Hi, impudent " + family.getPet().getNickname());
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void repairCar() {
        System.out.println("It is done! The car is repiared!");
    }
}
