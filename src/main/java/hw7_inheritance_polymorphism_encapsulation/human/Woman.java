package hw7_inheritance_polymorphism_encapsulation.human;

import hw7_inheritance_polymorphism_encapsulation.Family;

import java.util.Calendar;
import java.util.Random;

final public class Woman extends Human implements HumanCreate {

    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }


    public Woman(String name, String surname, int year, int iq, Family family) {
        super(name, surname, year, iq, family);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet() {
        if (family != null && family.getPet() != null) {
            System.out.println("Hello, my dear " + family.getPet().getNickname());
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void makeup() {
        System.out.println("Great! Now I am even more beautiful");
    }

    @Override
    public void bornChild() {
        Random random = new Random();
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);

        if (random.nextInt(101) % 2 > 0) {
            BoysNames[] names = BoysNames.values();
            int number = random.nextInt(names.length);
            int iqOfChild = (this.family.getFather().getIq() + iq) / 2;
            Man child = new Man(names[number].toString(), this.family.getFather().surname, year, iqOfChild, this.family);
            this.family.addChild(child);
        } else {
            GirlsNames[] names = GirlsNames.values();
            int number = random.nextInt(names.length);
            int iqOfChild = (this.family.getFather().getIq() + this.iq) / 2;
            Woman child = new Woman(names[number].toString(), this.family.getFather().surname, year, iqOfChild, this.family);
            this.family.addChild(child);
        }
    }
}
