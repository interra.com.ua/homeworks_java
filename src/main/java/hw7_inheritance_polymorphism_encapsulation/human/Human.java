package hw7_inheritance_polymorphism_encapsulation.human;

import hw7_inheritance_polymorphism_encapsulation.Family;

public class Human {
    protected String name;
    protected String surname;
    protected int year;
    protected int iq;
    protected String[][] schedule;
    protected Family family;

    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.family = family;
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        StringBuilder human = new StringBuilder();
        human.append("human{");
        if (name != null) {
            human.append("name='" + name + "'");
        } else human.append("name='undefined'");
        if (surname != null) {
            human.append(", surname='" + surname + "'");
        } else human.append(", surname='undefined'");
        if (year != 0) {
            human.append(", year=" + year);
        }
        if (iq != 0) {
            human.append(", iq=" + getIq());
        }
        if (schedule != null && schedule.length > 0) {
            human.append(", schedule=[" + getSchedule() + "]");
        }
        human.append("}");

        return human.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;
        Human human = (Human) obj;
        return (name.equals(human.name) && surname.equals(human.surname) && year == human.year);
    }

    @Override
    public int hashCode() {
        int code = 11;
        int k = 7;
        code = k * code + name.hashCode() + surname.hashCode() + year;
        return code;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    public String getName() {
        return name;
    }


    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public int getIq() {
        return iq;
    }

    public String getSchedule() {
        String scheduleToString = "";

        for (int i = 0; i < schedule.length; i++) {
            scheduleToString = scheduleToString + "[";
            for (int j = 0; j < schedule[0].length; j++) {
                if (j == 0) {
                    scheduleToString = scheduleToString + schedule[i][j].substring(0, 1).toUpperCase() + schedule[i][j].substring(1).toLowerCase();
                    scheduleToString = scheduleToString + ", ";
                } else {
                    scheduleToString = scheduleToString + schedule[i][j];
                }
            }
            if (i != schedule.length - 1) {
                scheduleToString = scheduleToString + "], ";
            } else {
                scheduleToString = scheduleToString + "]";
            }
        }

        return scheduleToString;
    }

    public void greetPet() {
        if (family != null && family.getPet() != null) {
            System.out.println("Hello, " + family.getPet().getNickname());
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void describePet() {
        if (family != null && family.getPet() != null) {
            System.out.printf("\"I have %s, it is %d years old, it is %s.%n\"", family.getPet().getSpecies(), family.getPet().getAge(), ((family.getPet().getTrickLevel() > 50) ? "very cunning" : "almost not cunning"));
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }

    }

    public void feedPet() {
        if (family != null && family.getPet() != null) {
            System.out.printf("My pet %s, what are you doing? %n", family.getPet().getNickname());
            family.getPet().eat();
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }

    }


}
