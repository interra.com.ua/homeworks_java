package hw7_inheritance_polymorphism_encapsulation;

import hw7_inheritance_polymorphism_encapsulation.human.Man;
import hw7_inheritance_polymorphism_encapsulation.human.Woman;
import hw7_inheritance_polymorphism_encapsulation.pets.DomesticCat;

import java.util.Arrays;

class Main {

    public static void main(String[] args) {

        String[][] h = {{DayOfWeek.MONDAY.name(), "sleep"}, {DayOfWeek.TUESDAY.name(), "sleep"}, {DayOfWeek.WEDNESDAY.name(), "sleep"}, {DayOfWeek.THURSDAY.name(), "sleep"}, {DayOfWeek.FRIDAY.name(), "sleep"}, {DayOfWeek.SATURDAY.name(), "sleep"}, {DayOfWeek.SUNDAY.name(), "sleep"}};
        Man zorro = new Man("Zorro", "de la Vega", 1970, 85, h);
        Woman wifeOfZorro = new Woman("Elena", "de la Vega", 1975, 79, h);
        Woman daughterOfZorro = new Woman("Alice", "de la Vega", 1997);
        Man sunOfZorro = new Man("Hoakin", "de la Vega", 2000, 86, h);

        Family deLaVega = new Family(wifeOfZorro, zorro);

        String[] DomesticCatsHabbits = {"sleep", "eat", "scratch"};
        DomesticCat cat1 = new DomesticCat("Vasiliy", 5, 75, DomesticCatsHabbits, deLaVega);

        cat1.foul();
        cat1.respond();
        System.out.println(cat1.toString());

        wifeOfZorro.bornChild();
        System.out.println(Arrays.toString(wifeOfZorro.getFamily().getChildren()));
        System.out.println(zorro.toString());
    }
}
