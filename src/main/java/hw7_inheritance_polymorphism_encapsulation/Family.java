package hw7_inheritance_polymorphism_encapsulation;

import hw7_inheritance_polymorphism_encapsulation.human.Human;
import hw7_inheritance_polymorphism_encapsulation.pets.Pet;

import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);

    }

    @Override
    public String toString() {
        StringBuilder family = new StringBuilder();

        family.append("family{" +
                "mother=" + mother +
                ", father=" + father);
        if (children.length != 0) {
            family.append(Arrays.toString(children));
        }
        if (pet != null) {
            family.append(pet);
        }

        return family.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Pet getPet() {
        return pet;
    }

    public void addChild(Human child) {
        if (children != null) {
            Human[] array = new Human[children.length + 1];
            System.arraycopy(children, 0, array, 0, children.length);
            array[array.length - 1] = child;
            children = array;
        } else {
            children = new Human[]{child};
        }

        child.setFamily(this);
    }

    public boolean deleteChild(int index) {

        if (children != null && index >= 0 && index < children.length) {
            Human[] array = new Human[children.length - 1];
            for (int i = 0; i < array.length; i++) {
                if (i < index) {
                    array[i] = children[i];
                } else {
                    array[i] = children[i + 1];
                }
            }
            children = array;
            return true;
        }
        return false;
    }

    public boolean deleteChild(Human child) {

        if (children != null) {
            for (int i = 0; i < children.length; i++) {
                if (children[i].equals(child)) {
                    Human[] array = new Human[children.length - 1];
                    System.arraycopy(children, 0, array, 0, i);
                    System.arraycopy(children, (i + 1), array, i, (array.length - i));
                    children = array;
                    return true;
                }
            }
        }
        return false;
    }


    int countFamily() {
        return 2 + children.length;
    }
}
