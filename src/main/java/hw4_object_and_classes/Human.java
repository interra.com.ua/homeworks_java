package hw4_object_and_classes;



public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] shedule;

    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }


    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] shedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.shedule = shedule;
    }

    @Override
    public String toString(){
        StringBuilder human = new StringBuilder();
        human.append("human{");
        if(name != null){
            human.append("name='" + name + "'");
        } else human.append ("name='undefined'") ;
        if(surname != null){
            human.append(", surname='" + surname + "'");
        } else human.append (", surname='undefined'") ;
        if(year != 0){
            human.append(", year=" + year);
        }
        if(iq != 0){
            human.append(", iq=" + iq);
        }
        if(mother != null){
            human.append(", mother=" + mother.name + " " + mother.surname);
        }
        if(father != null){
            human.append(", father=" + father.name + " " + father.surname);
        }
        if(pet != null){
            human.append(", pet=" + pet.toString());
        }
        human.append("}");

        return human.toString();
//         return "human{name='" + name + "', surname='" + surname + "', year=" + year + ", iq=" + iq + ", mother=" + mother +", father=" + father + ", pet=" + pet.toString();
    }

    public String getName() {
        return name;
    }

    public void greetPet(){
        System.out.println("Привет, " + pet.getNickname());
    }

    public void describePet(){
        System.out.printf("У меня есть %s, ему %d лет, он %s.%n", pet.getSpecies(), pet.getAge(), ((pet.getTrickLevel() > 50)?"очень хитрый" : "почти не хитрый"));
    }



}
