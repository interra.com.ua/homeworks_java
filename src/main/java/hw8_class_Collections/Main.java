package hw8_class_Collections;

import hw8_class_Collections.human.Human;
import hw8_class_Collections.human.Man;
import hw8_class_Collections.human.Woman;
import hw8_class_Collections.pets.Dog;
import hw8_class_Collections.pets.DomesticCat;

import java.util.EnumMap;

class Main {

    public static void main(String[] args) {

        EnumMap<DayOfWeek, String> schedule = new EnumMap<>(DayOfWeek.class);
        schedule.put(DayOfWeek.MONDAY, "monday training");
        schedule.put(DayOfWeek.TUESDAY, "tuesday chill");
        schedule.put(DayOfWeek.WEDNESDAY, "wednesday meeting");

        Man zorro = new Man("Zorro", "de la Vega", 1970, 85, schedule);
        Woman wifeOfZorro = new Woman("Elena", "de la Vega", 1975, 79, schedule);
        Woman daughterOfZorro = new Woman("Alice", "de la Vega", 1997);

        DomesticCat cat1 = new DomesticCat("Vasiliy", 5, 75, "sleep", "eat", "scratch");
        Dog dog1 = new Dog("Polkan", 3, 85, "play", "love", "serve");

        Family deLaVega = new Family(wifeOfZorro, zorro);
        deLaVega.addChild(daughterOfZorro);
        Human childOfZorro1 = wifeOfZorro.bornChild();
        deLaVega.addPet(cat1);
        deLaVega.addPet(dog1);

        System.out.println(zorro);
        System.out.println(deLaVega);
        childOfZorro1.describePets();

        zorro.greetPets();
        zorro.feedPet(dog1);

        wifeOfZorro.greetPets();

        deLaVega.deleteChild(1);
        System.out.println(deLaVega);

        deLaVega.deleteChild(daughterOfZorro);
        System.out.println(deLaVega);

    }
}
