package hw8_class_Collections.pets;

public enum Species {
    DOG,
    DOMESTICCAT,
    FISH,
    BIRD,
    RABBIT,
    TURTLE,
    SNAKE,
    ROBOCAT,
    UNKNOWN
}
