package hw8_class_Collections.pets;

import hw8_class_Collections.Family;

import java.util.HashSet;

public class DomesticCat extends Pet implements Foul {

    public DomesticCat(String nickname) {
        super(nickname);
        super.species = Species.DOMESTICCAT;
    }

    public DomesticCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.DOMESTICCAT;
    }

    public DomesticCat(String nickname, int age, int trickLevel, HashSet<String> habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
        super.species = Species.DOMESTICCAT;
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am the cat " + super.nickname + ". Why do you stand and look at me? Have you brought Whiskas?..");
    }

    @Override
    public void foul() {
        System.out.println("Okay I slept now I can go demand food...");
    }
}
