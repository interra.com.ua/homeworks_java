package hw8_class_Collections.pets;

public class Fish extends Pet {

    public Fish(String nickname) {
        super(nickname);
        super.species = Species.FISH;
    }


    @Override
    public void respond() {
        System.out.println("I am the fish " + super.nickname + "I can say only \"Mmmmmm.. mmm.. mm\"");
    }
}
