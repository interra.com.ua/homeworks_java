package hw8_class_Collections;

import hw8_class_Collections.human.Human;
import hw8_class_Collections.pets.Pet;

import java.util.ArrayList;
import java.util.HashSet;

public class Family {
    private Human mother;
    private Human father;
    private ArrayList<Human> children = new ArrayList<>();
    private HashSet<Pet> pets = new HashSet<>();

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
    }

    @Override
    public String toString() {
        StringBuilder family = new StringBuilder();

        family.append("family{" +
                "mother=" + mother +
                ", father=" + father);
        if (!children.isEmpty()) {
            family.append(children);
        }
        if (!pets.isEmpty()) {
            family.append(pets);
        }

        return family.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
        pet.setFamily(this);
    }

    public HashSet<Pet> getPets() {
        return pets;
    }

    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {
        return index >= 0 && index < children.size() && children.remove(index) != null;
    }

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }


    int countFamily() {
        return 2 + children.size();
    }
}
