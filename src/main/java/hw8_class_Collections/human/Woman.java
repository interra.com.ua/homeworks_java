package hw8_class_Collections.human;

import hw8_class_Collections.DayOfWeek;
import hw8_class_Collections.Family;
import hw8_class_Collections.pets.Pet;

import java.util.Calendar;
import java.util.EnumMap;
import java.util.Random;

final public class Woman extends Human implements HumanCreate {

    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }


    public Woman(String name, String surname, int year, int iq, Family family) {
        super(name, surname, year, iq, family);
    }

    public Woman(String name, String surname, int year, int iq, EnumMap<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPets() {
        if (family != null && !family.getPets().isEmpty()) {
            String nickNamesList = "";
            for (Pet pet : family.getPets()) {
                nickNamesList = nickNamesList + pet.getSpecies().toString().toLowerCase() + " " + pet.getNickname() + ", ";
            }

            System.out.println("Hello, " + nickNamesList + ", I love you!");
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void makeup() {
        System.out.println("Great! Now I am even more beautiful");
    }

    @Override
    public Human bornChild() {
        Random random = new Random();
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        Human addedChild;

        if (random.nextInt(101) % 2 > 0) {
            BoysNames[] names = BoysNames.values();
            int number = random.nextInt(names.length);
            int iqOfChild = (this.family.getFather().getIq() + iq) / 2;
            Man child = new Man(names[number].toString(), this.family.getFather().surname, year, iqOfChild, this.family);
            this.family.addChild(child);
            addedChild = child;
        } else {
            GirlsNames[] names = GirlsNames.values();
            int number = random.nextInt(names.length);
            int iqOfChild = (this.family.getFather().getIq() + this.iq) / 2;
            Woman child = new Woman(names[number].toString(), this.family.getFather().surname, year, iqOfChild, this.family);
            this.family.addChild(child);
            addedChild = child;
        }
        return addedChild;
    }
}
