package hw8_class_Collections.human;

import hw8_class_Collections.DayOfWeek;
import hw8_class_Collections.Family;
import hw8_class_Collections.pets.Pet;

import java.util.EnumMap;

final public class Man extends Human {

    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Family family) {
        super(name, surname, year, iq, family);
    }

    public Man(String name, String surname, int year, int iq, EnumMap<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPets() {
        if (family != null && !family.getPets().isEmpty()) {
            String nickNamesList = "";
            for (Pet pet : family.getPets()) {
                nickNamesList = nickNamesList + pet.getNickname() + ", ";
            }
            System.out.println("Hi, impudent " + nickNamesList + "would you like food?");
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void repairCar() {
        System.out.println("It is done! The car is repiared!");
    }
}
