package hw9_DAO_Service_layer_pattern.human;

import hw9_DAO_Service_layer_pattern.DayOfWeek;
import hw9_DAO_Service_layer_pattern.family.Family;
import hw9_DAO_Service_layer_pattern.pets.Pet;

import java.util.EnumMap;
import java.util.Map;

public class Human {
    protected String name;
    protected String surname;
    protected int year;
    protected int iq;
    protected EnumMap<DayOfWeek, String> schedule;
    protected Family family;

    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.family = family;
    }

    public Human(String name, String surname, int year, int iq, EnumMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        StringBuilder human = new StringBuilder();
        human.append("human{");
        if (name != null) {
            human.append("name='" + name + "'");
        } else human.append("name='undefined'");
        if (surname != null) {
            human.append(", surname='" + surname + "'");
        } else human.append(", surname='undefined'");
        if (year != 0) {
            human.append(", year=" + year);
        }
        if (iq != 0) {
            human.append(", iq=" + getIq());
        }
        if (schedule != null && !schedule.isEmpty()) {
            human.append(", schedule=[" + getSchedule() + "]");
        }
        human.append("}");

        return human.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;
        Human human = (Human) obj;
        return (name.equals(human.name) && surname.equals(human.surname) && year == human.year);
    }

    @Override
    public int hashCode() {
        int code = 11;
        int k = 7;
        code = k * code + name.hashCode() + surname.hashCode() + year;
        return code;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public int getIq() {
        return iq;
    }

    public int birthDate() {
        return year;
    }

    public String getSchedule() {
        String scheduleToString = "";
        int i = 0;

        for (Map.Entry<DayOfWeek, String> keyValue : schedule.entrySet()) {
            i++;
            scheduleToString = scheduleToString + "[" + keyValue.getKey().toString().substring(0, 1).toUpperCase() + keyValue.getKey().toString().substring(1).toLowerCase() + ", " + keyValue.getValue();
            if (i == schedule.entrySet().size()) {
                scheduleToString = scheduleToString + "]";
            } else {
                scheduleToString = scheduleToString + "], ";
            }
        }
        return scheduleToString;
    }

    public void greetPets() {
        if (family != null && !family.getPets().isEmpty()) {
            String nickNamesList = "";
            for (Pet pet : family.getPets()) {
                nickNamesList = nickNamesList + pet.getNickname() + ", ";
            }

            System.out.println("Hello, " + nickNamesList + "! How are you?");
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void describePets() {
        if (family != null && !family.getPets().isEmpty()) {
            for (Pet pet : family.getPets()) {
                String species = pet.getSpecies().toString().substring(0, 1).toUpperCase() + pet.getSpecies().toString().substring(1).toLowerCase();
                System.out.printf("I have %s, it is %d years old, it is %s.%n", species, pet.getAge(), ((pet.getTrickLevel() > 50) ? "very cunning" : "almost not cunning"));
            }
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }

    }

    public void feedPet(Pet pet) {
        if (family != null && !family.getPets().isEmpty() && family.getPets().contains(pet)) {
            System.out.printf("My pet %s, what are you doing? %n", pet.getNickname());
            pet.eat();
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }

    }
}
