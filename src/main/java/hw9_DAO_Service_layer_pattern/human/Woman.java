package hw9_DAO_Service_layer_pattern.human;

import hw9_DAO_Service_layer_pattern.DayOfWeek;
import hw9_DAO_Service_layer_pattern.family.Family;
import hw9_DAO_Service_layer_pattern.pets.Pet;

import java.util.Calendar;
import java.util.EnumMap;
import java.util.Random;

final public class Woman extends Human implements HumanCreate {

    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }


    public Woman(String name, String surname, int year, int iq, Family family) {
        super(name, surname, year, iq, family);
    }

    public Woman(String name, String surname, int year, int iq, EnumMap<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPets() {
        if (family != null && !family.getPets().isEmpty()) {
            String nickNamesList = "";
            for (Pet pet : family.getPets()) {
                nickNamesList = nickNamesList + pet.getSpecies().toString().toLowerCase() + " " + pet.getNickname() + ", ";
            }

            System.out.println("Hello, \" + nickNamesList + \", I love you!");
        } else {
            System.out.printf("Unfortunately, there are no pets in our family %s and i can't greet them %n", surname);
        }
    }

    public void makeup() {
        System.out.println("Great! Now I am even more beautiful");
    }

    @Override
    public Human bornChild(BoysNames boysName, GirlsNames femaleName) {
        Random random = new Random();
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        Human addedChild;

        if (random.nextInt(101) % 2 > 0) {
            int iqOfChild = (this.family.getFather().getIq() + iq) / 2;
            Man child = new Man(boysName.toString(), this.family.getFather().surname, year, iqOfChild, this.family);
            this.family.addChild(child);
            addedChild = child;
        } else {
            int iqOfChild = (this.family.getFather().getIq() + this.iq) / 2;
            Woman child = new Woman(femaleName.toString(), this.family.getFather().surname, year, iqOfChild, this.family);
            this.family.addChild(child);
            addedChild = child;
        }
        return addedChild;
    }
}
