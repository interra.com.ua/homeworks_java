package hw9_DAO_Service_layer_pattern.human;

public interface HumanCreate {
    Human bornChild(BoysNames boysName, GirlsNames femaleName);
}
