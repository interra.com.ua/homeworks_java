package hw9_DAO_Service_layer_pattern;

import hw9_DAO_Service_layer_pattern.controller.FamilyController;
import hw9_DAO_Service_layer_pattern.human.*;
import hw9_DAO_Service_layer_pattern.pets.Dog;
import hw9_DAO_Service_layer_pattern.pets.DomesticCat;

class Main {

    public static void main(String[] args) {
        Man zorro1 = new Man("Zorro1", "Family1", 1970);
        Woman wifeOfZorro1 = new Woman("Elena1", "Family1", 1975);
        Man zorro2 = new Man("Zorro2", "Family2", 1970);
        Woman wifeOfZorro2 = new Woman("Elena2", "Family2", 1975);
        Man zorro3 = new Man("Zorro3", "Family3", 1970);
        Woman wifeOfZorro3 = new Woman("Elena3", "Family3", 1975);
        Human child1 = new Human("Person1", "Surname1", 1986);
        Human child2 = new Human("Person2", "Surname2", 2003);
        Human child3 = new Human("Person3", "Surname3", 2000);
        Dog dog1 = new Dog("Polkan1", 3, 85, "play", "love", "serve");
        Dog dog2 = new Dog("Polkan2", 3, 85, "play", "love", "serve");
        DomesticCat cat = new DomesticCat("Vas'ka");

        FamilyController familyController = new FamilyController();

        familyController.createNewFamily(zorro1, wifeOfZorro1);
        familyController.createNewFamily(zorro2, wifeOfZorro2);
        familyController.createNewFamily(zorro3, wifeOfZorro3);

        familyController.displayAllFamilies();

        System.out.printf("%n Children and pets were added: %n%n");

        familyController.adoptChild(zorro2.getFamily(), child2);
        familyController.adoptChild(zorro3.getFamily(), child3);
        familyController.bornChild(zorro3.getFamily(), BoysNames.VASYA, GirlsNames.LENA);
        familyController.addPet(0, dog1);
        familyController.addPet(1, dog2);
        familyController.addPet(0, cat);
        familyController.displayAllFamilies();

        System.out.println("Total families: " + familyController.count());
        System.out.println("\n" + "Families with less than 3 people: " + familyController.getFamiliesLessThan(3));
        System.out.println("\n" + "The number of families with 3 people:" + familyController.countFamiliesWithMemberNumber(3));
        System.out.println("\n" + "Families with more than 3 people: " + familyController.getFamiliesBiggerThan(3));
        System.out.printf("%nSecond family in the list:" + familyController.getFamilyById(1));
        familyController.deleteFamily(1);
        System.out.printf("%nAfter its removal, the general list of families:");
        familyController.displayAllFamilies();
        System.out.printf("%nNow the child * Person3 * from the family * Family3 *, who was over 18 years old, formed his family and left his parents:");
        familyController.deleteAllChildrenOlderThen(18);
        familyController.displayAllFamilies();
        System.out.printf("%nWell, zorro1 did not get along with his wife for a long time and they divorced. Now the general list of families looks like this:%n");
        familyController.deleteFamily(zorro1.getFamily());
        familyController.displayAllFamilies();
        System.out.printf("%nIt happens =((%n");
    }
}
