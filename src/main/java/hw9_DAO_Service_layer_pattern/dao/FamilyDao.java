package hw9_DAO_Service_layer_pattern.dao;

import hw9_DAO_Service_layer_pattern.family.Family;

import java.util.List;

public interface FamilyDao {
    void addFamily(Family o);

    List getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family o);

    void saveFamily(Family o);
}
