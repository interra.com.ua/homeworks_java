package hw9_DAO_Service_layer_pattern.family;

import hw9_DAO_Service_layer_pattern.human.Human;
import hw9_DAO_Service_layer_pattern.human.Man;
import hw9_DAO_Service_layer_pattern.human.Woman;
import hw9_DAO_Service_layer_pattern.pets.Pet;

import java.util.ArrayList;
import java.util.HashSet;

public class Family {
    private Woman mother;
    private Man father;
    private ArrayList<Human> children = new ArrayList<>();
    private HashSet<Pet> pets = new HashSet<>();

    public Family(Man father, Woman mother) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);

    }

    @Override
    public String toString() {
        StringBuilder family = new StringBuilder();

        family.append("family{" +
                "mother=" + mother +
                ", father=" + father);
        if (!children.isEmpty()) {
            family.append(children);
        }
        if (!pets.isEmpty()) {
            family.append(pets);
        }

        return family.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    public Woman getMother() {
        return mother;
    }

    public Man getFather() {
        return father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {
        return index >= 0 && index < children.size() && children.remove(index) != null;
    }

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
        pet.setFamily(this);
    }

    public HashSet<Pet> getPets() {
        return pets;
    }

    public int countFamily() {
        return 2 + children.size();
    }
}
