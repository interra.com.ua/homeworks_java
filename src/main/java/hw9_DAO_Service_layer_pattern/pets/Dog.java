package hw9_DAO_Service_layer_pattern.pets;

import hw9_DAO_Service_layer_pattern.family.Family;

import java.util.HashSet;

public class Dog extends Pet implements Foul {
    public Dog(String nickname) {
        super(nickname);
        super.species = Species.DOG;
    }

    public Dog(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.DOG;
    }

    public Dog(String nickname, int age, int trickLevel, HashSet<String> habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
        super.species = Species.DOG;
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am the dog \" + nickname + \". I've missed you!");
    }

    @Override
    public void foul() {
        System.out.println("Oh, I think I ate your new shoes .. I need to cover the evidence well...");
    }
}
