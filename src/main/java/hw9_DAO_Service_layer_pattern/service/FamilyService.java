package hw9_DAO_Service_layer_pattern.service;

import hw9_DAO_Service_layer_pattern.family.CollectionFamilyDao;
import hw9_DAO_Service_layer_pattern.family.Family;
import hw9_DAO_Service_layer_pattern.human.*;
import hw9_DAO_Service_layer_pattern.pets.Pet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;

public class FamilyService {
    private CollectionFamilyDao familyDao = new CollectionFamilyDao();

    public void createNewFamily(Man father, Woman mother) {
        Family family = new Family(father, mother);
        familyDao.addFamily(family);
    }

    public ArrayList<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        for (Family o : familyDao.getAllFamilies()) {
            System.out.print(familyDao.getAllFamilies().indexOf(o) + "). Father: " + o.getFather().getSurname() + " " + o.getFather().getName()
                    + ", mother: " + o.getMother().getSurname() + " " + o.getMother().getName());
            if (!o.getChildren().isEmpty()) {
                System.out.print(", children: ");
                for (Human child : o.getChildren()) {
                    System.out.print(child.getSurname() + " " + child.getName());
                    if (o.getChildren().indexOf(child) == (o.getChildren().size() - 1)) {
                        System.out.print("");
                    } else {
                        System.out.print(", ");
                    }
                }
            }
            System.out.println(".");
        }
    }

    public ArrayList<Family> getFamiliesBiggerThan(int val) {
        ArrayList<Family> familiesBiggerThan = new ArrayList<>();
        for (Family o : familyDao.getAllFamilies()) {
            if (o.countFamily() > val) {
                familiesBiggerThan.add(o);
            }
        }
        return familiesBiggerThan;
    }

    public ArrayList<Family> getFamiliesLessThan(int val) {
        ArrayList<Family> familiesLessThan = new ArrayList<>();
        for (Family o : familyDao.getAllFamilies()) {
            if (o.countFamily() < val) {
                familiesLessThan.add(o);
            }
        }
        return familiesLessThan;
    }

    public int countFamiliesWithMemberNumber(int val) {
        int count = 0;
        for (Family o : familyDao.getAllFamilies()) {
            if (o.countFamily() == val) {
                count++;
            }
        }
        return count;
    }


    public void deleteFamily(int index) {
        familyDao.deleteFamily(index);
    }

    public void deleteFamily(Family o) {
        familyDao.deleteFamily(o);
    }

    public Family bornChild(Family o, BoysNames boysName, GirlsNames femaleName) {
        o.getMother().bornChild(boysName, femaleName);
        return o;
    }

    public Family adoptChild(Family o, Human child) {
        int familyIndex = familyDao.getAllFamilies().indexOf(o);
        familyDao.getFamilyByIndex(familyIndex).addChild(child);
        return familyDao.getFamilyByIndex(familyIndex);
    }

    public void deleteAllChildrenOlderThen(int age) {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);

        for (Family family : familyDao.getAllFamilies()) {
            family.getChildren().removeIf(child -> ((year - child.birthDate() > age)));
        }
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int familyIndex) {
        return familyDao.getAllFamilies().get(familyIndex);
    }

    public HashSet<Pet> getPets(int familyIndex) {
        return familyDao.getAllFamilies().get(familyIndex).getPets();
    }

    public void addPet(int indexFamily, Pet o) {
        familyDao.getAllFamilies().get(indexFamily).addPet(o);
    }

    public void saveFamily(Family o) {
        familyDao.saveFamily(o);
    }

}
