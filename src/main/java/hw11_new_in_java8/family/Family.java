package hw11_new_in_java8.family;

import hw11_new_in_java8.human.Human;
import hw11_new_in_java8.human.Man;
import hw11_new_in_java8.human.Woman;
import hw11_new_in_java8.pets.Pet;

import java.util.ArrayList;
import java.util.HashSet;

public class Family {
    private Woman mother;
    private Man father;
    private ArrayList<Human> children = new ArrayList<>();
    private HashSet<Pet> pets = new HashSet<>();

    public Family(Man father, Woman mother) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);

    }

    @Override
    public String toString() {
        StringBuilder family = new StringBuilder();

        family.append("family{mother=").append(mother).append(", father=").append(father);

        if (!children.isEmpty()) {
            family.append(children);
        }
        if (!pets.isEmpty()) {
            family.append(pets);
        }

        return family.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString());
    }

    public Woman getMother() {
        return mother;
    }

    public Man getFather() {
        return father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {
        return index >= 0 && index < children.size() && children.remove(index) != null;
    }

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
        pet.setFamily(this);
    }

    public HashSet<Pet> getPets() {
        return pets;
    }

    public int countFamily() {
        return 2 + children.size();
    }
}
