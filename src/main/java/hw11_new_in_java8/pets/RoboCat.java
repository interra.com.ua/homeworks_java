package hw11_new_in_java8.pets;

public class RoboCat extends Pet {
    @Override
    public void respond() {
        System.out.println("I am a ro-bot \" + super.nickname + \". I ca-me-in-peace. I need your clo-thes");
    }
}
