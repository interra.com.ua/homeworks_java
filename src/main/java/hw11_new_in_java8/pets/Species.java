package hw11_new_in_java8.pets;

public enum Species {
    DOG,
    DOMESTICCAT,
    FISH,
    BIRD,
    RABBIT,
    TURTLE,
    SNAKE,
    ROBOCAT,
    UNKNOWN
}
