package hw11_new_in_java8.dao;

import hw11_new_in_java8.family.Family;

import java.util.List;

public interface FamilyDao {

    void addFamily(Family o);

    List getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family o);

    void saveFamily(Family o);
}
