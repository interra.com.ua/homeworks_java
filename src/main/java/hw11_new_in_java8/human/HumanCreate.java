package hw11_new_in_java8.human;

public interface HumanCreate {
    Human bornChild(BoysNames boysName, GirlsNames femaleName);
}
