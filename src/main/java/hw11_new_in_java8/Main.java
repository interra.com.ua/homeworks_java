package hw11_new_in_java8;

import hw11_new_in_java8.human.Human;
import hw11_new_in_java8.human.Man;
import hw11_new_in_java8.human.Woman;
import hw11_new_in_java8.сontroller.FamilyController;

class Main {

    public static void main(String[] args) {
        Man zorro1 = new Man("Zorro1", "Family1", "15/12/1970");
        Man zorro2 = new Man("Zorro2", "Family2", "15/12/1970");
        Man zorro3 = new Man("Zorro3", "Family3", "15/12/1970");

        Woman wifeOfZorro1 = new Woman("Elena1", "Family1", "12/05/1975");
        Woman wifeOfZorro2 = new Woman("Elena2", "Family2", "12/05/1975");
        Woman wifeOfZorro3 = new Woman("Elena3", "Family3", "12/05/1975");

        Human child1 = new Human("Person1", "Surname1", "08/04/1989");
        Human child2 = new Human("Person2", "Surname2", "04/06/2001");
        Human child3 = new Human("Person3", "Surname3", "10/11/2009");

        FamilyController fc = new FamilyController();
        fc.createNewFamily(zorro1, wifeOfZorro1);
        fc.createNewFamily(zorro2, wifeOfZorro2);
        fc.createNewFamily(zorro3, wifeOfZorro3);
        fc.adoptChild(zorro2.getFamily(), child1);
        fc.adoptChild(zorro3.getFamily(), child2);
        fc.adoptChild(zorro3.getFamily(), child3);

        fc.displayAllFamilies();

    }
}
