package hw3_String;

import java.util.Scanner;

public class TaskManager {

    private String[][] schedule = new String[7][2];

    public void createTaskManager(){
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do homework";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to zoo;";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to theater;";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to cinema;";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to forest with friends;";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "stay home;";

    }

    public String askUser(String ask){
        Scanner in = new Scanner(System.in);
        System.out.println(ask);
        return in.nextLine();
    }

    public String parseInputedCommand(String value) {

        if (value.toLowerCase().contains("exit".toLowerCase())) {
            return "Exit";
        }else if (value.toLowerCase().contains("change".toLowerCase()) || value.toLowerCase().contains("reschedule")) {
            return "Change";
        } else return "ReadTask";
    }

    public String parseInputedDay(String value){

        for (String[] day : schedule) {
            if (value.toLowerCase().contains(day[0].toLowerCase())) {
                return day[0];
            }
        }

        return "Mistake";
    }

    public void setTask(String day, String task) {
        switch (day) {
            case ("Sunday"):
                schedule[0][1] = task;
                break;
            case ("Monday"):
                schedule[1][1] = task;
                break;
            case ("Tuesday"):
                schedule[2][1] = task;
                break;
            case ("Wednesday"):
                schedule[3][1] = task;
                break;
            case ("Thursday"):
                schedule[4][1] = task;
                break;
            case ("Friday"):
                schedule[5][1] = task;
                break;
            case ("Saturday"):
                schedule[6][1] = task;
                break;

            default:
                System.out.println("You have inputed incorrect day name");
        }
    }

    public String getTask(String day) {
        switch (day) {
            case ("Sunday"):
                return schedule[0][1];
            case ("Monday"):
                return schedule[1][1];
            case ("Tuesday"):
                return schedule[2][1];
            case ("Wednesday"):
                return schedule[3][1];
            case ("Thursday"):
                return schedule[4][1];
            case ("Friday"):
                return schedule[5][1];
            case ("Saturday"):
                return schedule[6][1];

            default:
                return "You have inputed incorrect day name";

        }
    }
}
